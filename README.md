# Focus Devops Coding Test

This repo contains the commands needed to run this aplication

## Test Overview
The purpose of this test is to demonstrate your knowledge in the following areas: 
* Digitalocean Provider
* Digitalocean container registry 
* Terraform
* Kubernetes [Digitalocean kubernetes service] 
* Helm
* Gitlab CI 

## Installation

Before you run this aplication you need install Helm and Kubectl in your local machine

## Usage

### Helm
*Using gitlabCI you can deploy kubernetes service, convert kubeconfig file into base64 and put that value in a variable, if you wanna run this you can see my gitlab pipeline and test it